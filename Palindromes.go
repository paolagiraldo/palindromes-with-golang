package main

import(
	"fmt"
	"regexp"
	"strings"
)



const str = "16\nUe-Xa,iiunsyewfexkqzyobdo-uqqu-Odb o, Yz, qk xefweysnuiia X, Eu\nYarauer Hb-oaxo-U Oxaobmreu a r-Ay\nUa e, opoi, lqoxjbu-Ogugoubj, Xoyliopoeau\nYvrpedyqwezizewqyde Prvy\nAu, Y Yo, F Vw-Z, Zwvf Oyy ua\nIfoofucrhmuc-czewcodidocwezccu-mhrcuf-oo-Fi\nOoh-Qodmi Vov, o, Vi M-d Oqho, o\nReul, Locai-Fvaiyif, fof, I, s a, nuiunas i Foffi, Yiavfiacolluer\nGihfyp, jwm ogoyyogomwj, Py-fhig\nFajueyefmbl, Ezi pi, ueuisvyp, Py-Vsi-Ueuip-Izelb, mfeyeujaf\nT, ekb, H-Ojoh Bket\nOjr Jussa Yyitmpacoe, Idcexexecdieocapmtiyy Assujrja\nOu j, V Ita, n-Ayedeya,aN-ati v-Ju-O\nV, xome Za, emkyxttoehfod, tl-Yiyltdofheoztxykmeaze mo Xv\nReo, Of r, Ountdpi, B, O-w, rrwo Bipdtn uo-rfooer\nUoytooygkpkg Yoo ty, eu"
//const str = ""

var re = regexp.MustCompile(`[^\w]`)
var response []string

func main() {
	lines := strings.Split(str,"\n")

	for _, item := range lines[1:]{
		//fmt.Println(item)
		palindrome := isPalindrome(item)
		response = append(response, palindrome)

	}
	fmt.Println(response)
}

func isPalindrome(s string) string {
	rns:= []rune(s)
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {
		rns[i], rns[j] = rns[j], rns[i]
	}


	original := strings.ToLower(re.ReplaceAllString(s, ""))
	final := strings.ToLower(re.ReplaceAllString(string(rns), ""))
	//fmt.Println(original)
	//fmt.Println(final)
	if original==final{
		return "Y"
	}else {
		return "N"
	}


}


