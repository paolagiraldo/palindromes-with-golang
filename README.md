# Palindromes with Golang

Task: Palindromes (#50)
To determine, whether the phrase represents a palindrome or not.